package order

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/jakinss321/geotask/module/order/service"
)

const (
	orderCleanInterval = 5 * time.Second
)

// OrderCleaner воркер, который удаляет старые заказы
// используя метод orderService.RemoveOldOrders()
type OrderCleaner struct {
	orderService service.Orderer
}

func NewOrderCleaner(orderService service.Orderer) *OrderCleaner {
	return &OrderCleaner{orderService: orderService}
}

func (o *OrderCleaner) Run() {

	// Использовать горутину и select
	go func() {
		// Использовать time.NewTicker()
		ticker := time.NewTicker(orderCleanInterval)

		for range ticker.C {
			// Вызывать метод orderService.RemoveOldOrders()
			err := o.orderService.RemoveOldOrders(context.Background())
			if err != nil {
				// Если произошла ошибка, вывести ее в лог
				fmt.Println("Error removing old orders:", err)
			}
		}
	}()
}
