package order

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/jakinss321/geotask/geo"
	"gitlab.com/jakinss321/geotask/module/order/service"
)

const (
	// order generation interval
	orderGenerationInterval = 10 * time.Millisecond
	maxOrdersCount          = 200
)

// worker generates orders and put them into redis
type OrderGenerator struct {
	orderService service.Orderer
	allowedZone  *geo.Polygon
}

func NewOrderGenerator(orderService service.Orderer, allowedZone *geo.Polygon) *OrderGenerator {
	return &OrderGenerator{orderService: orderService, allowedZone: allowedZone}
}

func (o *OrderGenerator) Run() {
	// запускаем горутину для генерации заказов
	go func() {
		// Используем time.NewTicker()
		ticker := time.NewTicker(orderGenerationInterval)

		for range ticker.C {
			// Получаем количество заказов
			count, err := o.orderService.GetCount(context.Background())
			if err != nil {
				fmt.Println("Error getting order count:", err)
				continue
			}
			// Если количество заказов меньше, чем maxOrdersCount, генерируем новый заказ
			if count < maxOrdersCount {
				// Генерируем случайную точку внутри разрешенной зоны
				point := o.allowedZone.RandomPoint()

				// Создаем заказ только если точка внутри разрешенной зоны
				if o.allowedZone.Contains(point) {
					err = o.orderService.GenerateOrder(context.Background())
					if err != nil {
						fmt.Println("Error generating order:", err)
						continue
					}
				}
			}
		}
	}()
}
