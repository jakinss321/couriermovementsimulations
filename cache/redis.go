package cache

import (
	"time"

	"github.com/go-redis/redis"
)

func NewRedisClient(host, port string) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:         host + ":" + port,
		Password:     "",
		DB:           0,
		DialTimeout:  5 * time.Second,
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
	})
}
