package service

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/jakinss321/geotask/geo"
	"gitlab.com/jakinss321/geotask/module/order/models"
	"gitlab.com/jakinss321/geotask/module/order/storage"
)

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 120 * time.Second
)

type Orderer interface {
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // возвращает заказы через метод storage.GetByRadius
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохраняет заказ через метод storage.Save с заданным временем жизни OrderMaxAge
	GetCount(ctx context.Context) (int, error)                                                      // возвращает количество заказов через метод storage.GetCount
	RemoveOldOrders(ctx context.Context) error                                                      // удаляет старые заказы через метод storage.RemoveOldOrders с заданным временем жизни OrderMaxAge
	GenerateOrder(ctx context.Context) error                                                        // генерирует заказ в случайной точке из разрешенной зоны, с уникальным id, ценой и ценой доставки
}

// OrderService реализация интерфейса Orderer
// в нем должны быть методы GetByRadius, Save, GetCount, RemoveOldOrders, GenerateOrder
// данный сервис отвечает за работу с заказами
type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}

func (s *OrderService) GenerateOrder(ctx context.Context) error {
	point := s.allowedZone.RandomPoint()
	id := rand.Intn(1000000) + 1
	price := minDeliveryPrice + rand.Float64()*(maxDeliveryPrice-minDeliveryPrice)
	deliveryPrice := minOrderPrice + rand.Float64()*(maxOrderPrice-minOrderPrice)

	order := models.Order{
		ID:            int64(id),
		Price:         price,
		DeliveryPrice: deliveryPrice,
		Lng:           point.Lng,
		Lat:           point.Lat,
		IsDelivered:   false,
		CreatedAt:     time.Now(),
	}

	return s.storage.Save(ctx, order, orderMaxAge)
}

func (s *OrderService) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	return s.storage.Save(ctx, order, maxAge)
}

func (s *OrderService) RemoveOldOrders(ctx context.Context) error {
	return s.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (s *OrderService) GetCount(ctx context.Context) (int, error) {
	return s.storage.GetCount(ctx)
}

func (s *OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	// Вызываем метод GetByRadius из storage, чтобы получить заказы внутри заданного радиуса
	return s.storage.GetByRadius(ctx, lng, lat, radius, unit)
}
