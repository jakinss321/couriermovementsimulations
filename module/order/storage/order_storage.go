package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/jakinss321/geotask/module/order/models"
)

type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // получить заказ по id
	GenerateUniqueID(ctx context.Context) (int, error)                                              // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage *sqlx.DB
}

func NewOrderStorage(storage *sqlx.DB) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {

	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	orders, err := o.getAllOrders(ctx)
	if err != nil {
		return err
	}
	oldTime := time.Now().Add(-maxAge)
	for _, order := range orders {

		if order.CreatedAt.Before(oldTime) {

			_, err := o.storage.Exec("DELETE FROM orders WHERE id = $1", order.ID)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var err error
	var order models.Order
	// Получаем заказ из базы данных по ID
	err = o.storage.Get(&order, "SELECT * FROM orders WHERE id = $1", orderID)
	if err != nil {
		return nil, err // Возвращаем ошибку, если произошла ошибка при получении заказа из базы данных
	}

	return &order, nil // Возвращаем заказ и nil в качестве ошибки
}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	// Сериализуем заказ в JSON

	jsonData, err := json.Marshal(order)
	if err != nil {
		return err
	}

	_, err = o.storage.Exec("INSERT INTO orders (id, data, lat, lng, created_at) VALUES ($1, $2, $3, $4, $5)", order.ID, jsonData, order.Lat, order.Lng, order.CreatedAt)

	return err
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// Получаем количество заказов в базе данных
	var count int
	err := o.storage.Get(&count, "SELECT COUNT(*) FROM orders")
	if err != nil {
		return 0, err // Возвращаем ошибку, если возникла ошибка при получении количества заказов
	}
	return count, nil // Возвращаем количество заказов
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	// Получаем список заказов в заданном радиусе
	orders, err := o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	if err != nil {
		return nil, err // Возвращаем ошибку, если произошла ошибка при получении списка заказов
	}

	return orders, nil
}

func (o *OrderStorage) getAllOrders(ctx context.Context) ([]models.Order, error) {

	var orders []models.Order
	rows, err := o.storage.Queryx("SELECT * FROM orders")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var order models.Order
		var jsonData []byte
		err = rows.Scan(&order.ID, &jsonData, &order.Lat, &order.Lng, &order.CreatedAt)
		if err != nil {
			return nil, err
		}

		err = json.Unmarshal(jsonData, &order)
		if err != nil {
			return nil, err
		}

		orders = append(orders, order)
	}

	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var orders []models.Order
	var radiusInMeters float64

	// Конвертация радиуса в зависимости от единиц измерения
	switch unit {
	case "km":
		radiusInMeters = radius * 1000
	case "mi":
		radiusInMeters = radius * 1609.34
	case "m":
		radiusInMeters = radius
	default:
		return nil, fmt.Errorf("invalid unit provided: %s", unit)
	}

	// Вычисляем квадрат радиуса для использования в запросе
	squaredRadius := radiusInMeters * radiusInMeters

	// Получаем заказы в заданном радиусе из базы данных
	query := `
	SELECT id, data, lat, lng, created_at FROM orders
	WHERE (POW((lat - $1), 2) + POW((lng - $2), 2)) * POW(111.321, 2) <= $3`
	rows, err := o.storage.Queryx(query, lat, lng, squaredRadius)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var order models.Order
		var jsonData []byte
		err = rows.Scan(&order.ID, &jsonData, &order.Lat, &order.Lng, &order.CreatedAt)
		if err != nil {
			return nil, err
		}

		err = json.Unmarshal(jsonData, &order)
		if err != nil {
			return nil, err
		}

		orders = append(orders, order)
	}

	return orders, nil
}
func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int, error) {
	// Генерируем случайное число от 1 до 1000
	id := rand.Intn(1000) + 1

	return id, nil
}
