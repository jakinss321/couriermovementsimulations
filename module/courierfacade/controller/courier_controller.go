package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/jakinss321/geotask/module/courierfacade/service"
)

var (
	requestCount = promauto.NewCounter(prometheus.CounterOpts{
		Name: "courier_facade_request_count",
		Help: "The total number of requests to the Courier Facade",
	})
	requestDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "courier_facade_request_duration_seconds",
		Help:    "The duration of requests to the Courier Facade",
		Buckets: prometheus.LinearBuckets(0.01, 0.05, 10), // 10 buckets, each 50ms wide.
	})
	repoCount = promauto.NewCounter(prometheus.CounterOpts{
		Name: "courier_facade_repo_count",
		Help: "The total number of calls to the repository",
	})
	repoDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Name:    "courier_facade_repo_duration_seconds",
		Help:    "The duration of calls to the repository",
		Buckets: prometheus.LinearBuckets(0.01, 0.05, 10), // 10 buckets, each 50ms wide.
	})
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	start := time.Now()
	// Установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)

	// Получить статус курьера из сервиса courierService, используя метод GetStatus
	status := c.courierService.GetStatus(ctx)
	requestCount.Inc()
	requestDuration.Observe(time.Since(start).Seconds())
	// Отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, gin.H{
		"status": status,
	})

}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	start := time.Now()
	var cm CourierMove

	// Получить данные из m.Data и десериализовать их в структуру CourierMove
	err := json.Unmarshal(m.Data.([]byte), &cm)
	if err != nil {
		fmt.Println("Error decoding WebSocket message:", err)
		return
	}
	repoCount.Inc()
	repoDuration.Observe(time.Since(start).Seconds())

}
