package service_test

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	models "gitlab.com/jakinss321/geotask/module/courierfacade/models"
)

// MockCourierFacer is a mock of CourierFacer interface.
type MockCourierFacer struct {
	ctrl     *gomock.Controller
	recorder *MockCourierFacerMockRecorder
}

// MockCourierFacerMockRecorder is the mock recorder for MockCourierFacer.
type MockCourierFacerMockRecorder struct {
	mock *MockCourierFacer
}

// NewMockCourierFacer creates a new mock instance.
func NewMockCourierFacer(ctrl *gomock.Controller) *MockCourierFacer {
	mock := &MockCourierFacer{ctrl: ctrl}
	mock.recorder = &MockCourierFacerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockCourierFacer) EXPECT() *MockCourierFacerMockRecorder {
	return m.recorder
}

// GetStatus mocks base method.
func (m *MockCourierFacer) GetStatus(ctx context.Context) models.CourierStatus {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetStatus", ctx)
	ret0, _ := ret[0].(models.CourierStatus)
	return ret0
}

// GetStatus indicates an expected call of GetStatus.
func (mr *MockCourierFacerMockRecorder) GetStatus(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetStatus", reflect.TypeOf((*MockCourierFacer)(nil).GetStatus), ctx)
}

// MoveCourier mocks base method.
func (m *MockCourierFacer) MoveCourier(ctx context.Context, direction, zoom int) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "MoveCourier", ctx, direction, zoom)
}

// MoveCourier indicates an expected call of MoveCourier.
func (mr *MockCourierFacerMockRecorder) MoveCourier(ctx, direction, zoom interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "MoveCourier", reflect.TypeOf((*MockCourierFacer)(nil).MoveCourier), ctx, direction, zoom)
}
