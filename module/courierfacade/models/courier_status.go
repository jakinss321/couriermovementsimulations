package models

import (
	cm "gitlab.com/jakinss321/geotask/module/courier/models"
	om "gitlab.com/jakinss321/geotask/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
