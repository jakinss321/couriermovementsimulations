package storage

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/jakinss321/geotask/module/courier/models"
)

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error
	GetOne(ctx context.Context) (*models.Courier, error)
	Update(*models.Courier) error
}

type CourierStorage struct {
	db *sqlx.DB
}

func NewCourierStorage(db *sqlx.DB) CourierStorager {
	return &CourierStorage{db: db}
}

func (s *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	query := `INSERT INTO couriers (score, lat, lng) VALUES (:score, :lat, :lng)`

	_, err := s.db.NamedExecContext(ctx, query, map[string]interface{}{
		"score": courier.Score,
		"lat":   courier.Location.Lat,
		"lng":   courier.Location.Lng,
	})

	return err
}

type CourierDB struct {
	ID    int64   `db:"id"`
	Score int     `db:"score"`
	Lat   float64 `db:"lat"`
	Lng   float64 `db:"lng"`
}

func (s *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	query := `SELECT score, lat, lng FROM couriers ORDER BY id LIMIT 1`
	var courier models.Courier
	err := s.db.QueryRowContext(ctx, query).Scan(&courier.Score, &courier.Location.Lat, &courier.Location.Lng)
	if err != nil {
		if err == sql.ErrNoRows {

			newCourier := models.Courier{
				Score:    0,
				Location: models.Point{Lat: 0, Lng: 0},
			}
			err := s.Save(ctx, newCourier)
			if err != nil {
				return nil, err
			}
			return &newCourier, nil
		}
		return nil, err
	}
	return &courier, nil
}

func (s *CourierStorage) Update(courier *models.Courier) error {
	query := `UPDATE couriers SET score = :score WHERE lat = :lat AND lng = :lng`
	_, err := s.db.NamedExec(query, map[string]interface{}{
		"score": courier.Score,
		"lat":   courier.Location.Lat,
		"lng":   courier.Location.Lng,
	})

	return err
}
