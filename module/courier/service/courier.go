package service

import (
	"context"
	"math"

	"gitlab.com/jakinss321/geotask/geo"
	"gitlab.com/jakinss321/geotask/module/courier/models"
	"gitlab.com/jakinss321/geotask/module/courier/storage"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {

	courier, err := c.courierStorage.GetOne(ctx)
	if err != nil {
		return nil, err
	}
	// Получаем наши зоны
	allowedZone := geo.NewAllowedZone()
	disAllowedZones := []geo.PolygonChecker{
		geo.NewDisAllowedZone1(),
		geo.NewDisAllowedZone2(),
	}

	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	if !geo.CheckPointIsAllowed(point, allowedZone, disAllowedZones) {
		// Курьер за пределами разрешенной зоны

		newLocation := geo.GetRandomAllowedLocation(allowedZone, disAllowedZones)

		courier.Location.Lat = newLocation.Lat
		courier.Location.Lng = newLocation.Lng

		err := c.courierStorage.Update(courier)
		if err != nil {
			return nil, err
		}
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// Точность перемещения
	precision := 0.001 / math.Pow(2, float64(zoom-14))

	// Получить наши зоны
	allowedZone := geo.NewAllowedZone()
	disAllowedZones := []geo.PolygonChecker{
		geo.NewDisAllowedZone1(),
		geo.NewDisAllowedZone2(),
	}

	var newLat, newLng float64
	point := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	newLat = point.Lat
	newLng = point.Lng

	switch direction {
	case 0: // Вверх
		newLat = point.Lat + precision
	case 1: // Вниз
		newLat = point.Lat - precision
	case 2: // Вправо
		newLng = point.Lng - precision
	case 3: // Влево
		newLng = point.Lng + precision
	}

	point.Lat = newLat
	point.Lng = newLng

	if geo.CheckPointIsAllowed(point, allowedZone, disAllowedZones) {
		// Курьер вышел за пределы
		newLocation := geo.GetRandomAllowedLocation(allowedZone, disAllowedZones)

		courier.Location.Lat = newLocation.Lat
		courier.Location.Lng = newLocation.Lng
	} else {
		courier.Location.Lat = point.Lat
		courier.Location.Lng = point.Lng
	}

	courier.Location.Lat = point.Lat
	courier.Location.Lng = point.Lng

	err := c.courierStorage.Update(&courier)
	if err != nil {
		return err
	}
	return nil
}
