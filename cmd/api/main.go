package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/jakinss321/geotask/run"
)

func main() {
	godotenv.Load()
	// инициализация приложения
	app := run.NewApp()
	logrus.Println("123123123123")

	// запуск приложения
	err := app.Run()
	// в случае ошибки выводим ее в лог и завершаем работу с кодом 2
	if err != nil {
		log.Println(fmt.Sprintf("error: %s", err))
		os.Exit(2)
	}
}
