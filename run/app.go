package run

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/jmoiron/sqlx"
	"gitlab.com/jakinss321/geotask/geo"
	cservice "gitlab.com/jakinss321/geotask/module/courier/service"
	cstorage "gitlab.com/jakinss321/geotask/module/courier/storage"
	"gitlab.com/jakinss321/geotask/module/courierfacade/controller"
	cfservice "gitlab.com/jakinss321/geotask/module/courierfacade/service"
	oservice "gitlab.com/jakinss321/geotask/module/order/service"
	ostorage "gitlab.com/jakinss321/geotask/module/order/storage"
	"gitlab.com/jakinss321/geotask/router"
	"gitlab.com/jakinss321/geotask/server"
	"gitlab.com/jakinss321/geotask/workers/order"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func NewPostgresDB(Host, Port, DBName, Username, Password string) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", "host=courierdb port=5432 user=postgres dbname=postgres password=root sslmode=disable")
	if err != nil {
		fmt.Println(err, "1231123131231231231231231231 ошибка коннекта к бд")
		return nil, err
	}

	err = db.Ping()

	if err != nil {
		fmt.Println(err, "1231123131231231231231231231 ошибка коннекта к бд")
		return nil, err
	}
	fmt.Println(err, "коннект есть")
	return db, nil
}

func (a *App) Run() error {
	// получение хоста и порта redis
	// host := os.Getenv("REDIS_HOST")
	// port := os.Getenv("REDIS_PORT")

	// инициализация клиента redis
	// rclient := cache.NewRedisClient(host, port)

	// инициализация контекста с таймаутом
	// ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	// defer cancel()
	// проверка доступности redis
	// _, err := rclient.Ping().Result()
	// if err != nil {
	// 	return err
	// }

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_NAME")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")

	db, err := NewPostgresDB(dbHost, dbPort, dbName, dbUser, dbPassword)
	if err != nil {
		log.Fatalf("ошибка коннекта к бд %s", err)
	}
	// db, err := gorm.Open("postgres", "host="+dbHost+" port="+dbPort+" dbname="+dbName+" user="+dbUser+" password="+dbPassword+" sslmode=disable")
	// if err != nil {
	// 	return err
	// }
	// db.AutoMigrate(&models.Order{})
	// db.AutoMigrate(&m.Courier{})
	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()

	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(db)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService, allowedZone)
	orderGenerator.Run()
	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(db)
	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Metrics(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	// serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
