package router

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/jakinss321/geotask/module/courierfacade/controller"
)

type Router struct {
	courier *controller.CourierController
}

func NewRouter(courier *controller.CourierController) *Router {

	return &Router{courier: courier}
}

func (r *Router) CourierAPI(router *gin.RouterGroup) {
	router.GET("/status", func(ctx *gin.Context) {
		r.courier.GetStatus(ctx)
	})

	router.GET("/ws", func(ctx *gin.Context) {
		r.courier.Websocket(ctx)
	})
}

func (r *Router) Swagger(router *gin.RouterGroup) {
	router.GET("/swagger", swaggerUI)
}

func (r *Router) Metrics(router *gin.RouterGroup) {
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))
}
